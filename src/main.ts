import "./main.css";
import { Options, UseMirage, Elements, GetResources, SetResources, Replace, Translate, IsLoading, CreateLoader, Loader } from "./entity";

export const useMirage = function useMirage(options: Options): UseMirage {

  const getResources = async function getResources(language: string): GetResources {
    const response: object = await fetch(`${options.service}/${language}`)
      .then((response) => { return response.json(); }).catch(() => {
        console.log("%cMirage - No se pudo obtener respuesta, asegurese de que el servicio existe.", "color: orange");
      });
    return response;
  };

  const setResources = async function setResources(translation: object): SetResources {
    const response: void = await new Promise<void>((resolve) => {
      const elements: Elements = document.querySelectorAll("[mirage-key]");
      const useSplit = (attribute: string | null, separator: string) => { return (attribute ? attribute.split(separator) : null); };
      const replace = (element: Element): Replace => {
        const keys = useSplit(element.getAttribute("mirage-key"), " ") || [];
        keys.forEach((key: string) => {
          const text: string = key.split(".").reduce((text, i) => { return text[i]; }, translation);
          if (typeof text === "string") {
            element.innerHTML = text;
          } else {
            console.log(`%cMirage - No se pudo encontrar una traduccion para el atributo "${key}".`, "color: orange");
          }
        });
      };
      resolve(elements.forEach(replace));
    });
    return response;
  };

  const isLoading = function isLoading(loading: boolean): IsLoading {
    if (options.loader === false) { return; }
    let loader: Loader = document.getElementById("mirage-loader");
    const createLoader = function createLoader(): CreateLoader {
      loader = document.createElement("div");
      loader.setAttribute("id", "mirage-loader");
      loader.classList.add("m-loader");
      document.body.appendChild(loader);
    };
    loader !== null ? loader : createLoader();
    if (loading) {
      loader.removeAttribute("style");
      loader.classList.remove("m-hidden");
    }
    if (!loading) {
      loader.classList.add("m-hidden");
      loader.addEventListener("transitionend", () => { loader.style.display = "none"; });
    }
  };

  const translate = async function translate(language: string): Translate {
    console.log("%cMirage - 1.1 (V)", "color: orange");
    if (typeof language === "string" && options.languages.includes(language)) {
      isLoading(true);
      document.documentElement.lang = language;
      await setResources(await getResources(language)).then(() => { isLoading(false); }).catch(() => { isLoading(false); });
    } else {
      console.log(`%cMirage - Se esperaba alguno de los siguientes parametros "${options.languages}". En su lugar se obtuvo "${language}".`, "color: orange");
    }
  };

  return { translate };

};
