export type Options = { languages: string[], service: string, loader: boolean };
export type UseMirage = { translate: (string: string) => Translate };
export type Elements = NodeListOf<Element>;
export type GetResources = Promise<object>;
export type SetResources = Promise<void>;
export type Replace = void;
export type IsLoading = void;
export type CreateLoader = void;
export type Loader = HTMLElement;
export type Translate = Promise<void>;