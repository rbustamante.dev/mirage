import { defineConfig } from 'vite';
import { resolve } from 'path';
import dts from "vite-plugin-dts";

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, "index.ts"),
      name: "mirage",
      formats: ["es", "umd"],
      fileName: (format) => `index.${format}.js`,
    },
    emptyOutDir: true,
    manifest: false,
    outDir: './build',
    rollupOptions: {
      output: {
        sourcemap: false,
        assetFileNames: "index.[ext]",
      }
    },
    reportCompressedSize: false,
    chunkSizeWarningLimit: 500,
    cssCodeSplit: false,
  },
  publicDir: false,
  clearScreen: false,
  plugins: [dts()],
})
